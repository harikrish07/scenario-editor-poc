import register from "preact-custom-element";
import { tw, setup } from "https://cdn.skypack.dev/twind";
import { Box, Input, Text, Heading, Select } from "../atoms";

export const ScenarioEditor = (props) => {
  const { theme: customTheme } = props;

  setup({
    preflight: (preflight) => {
      return {
        ".scenarioEditor": { fontFamily: "inherit" },
        ".scenarioEditor blockquote,.scenarioEditor dl,.scenarioEditor dd,.scenarioEditor h1,.scenarioEditor h2,.scenarioEditor h3,.scenarioEditor h4,.scenarioEditor h5,.scenarioEditor h6,.scenarioEditor hr,.scenarioEditor figure,.scenarioEditor p,.scenarioEditor pre,.scenarioEditor fieldset,.scenarioEditor ol,.scenarioEditor ul":
          {
            margin: "0",
          },
        ".scenarioEditor button": {
          backgroundColor: "transparent",
          backgroundImage: "none",
        },
        '.scenarioEditor button,.scenarioEditor [type="button"],.scenarioEditor [type="reset"],.scenarioEditor [type="submit"]':
          {
            WebkitAppearance: "button",
          },
        ".scenarioEditor button:focus": {
          outline: ["1px dotted", "5px auto -webkit-focus-ring-color"],
        },
        ".scenarioEditor fieldset,.scenarioEditor ol,.scenarioEditor ul,.scenarioEditor legend":
          {
            padding: "0",
          },
        ".scenarioEditor ol,.scenarioEditor ul": {
          listStyle: "none",
        },
        "*,::before,::after": {
          border: "0 solid #e2e8f0",
        },
        ".scenarioEditor hr": {
          height: "0",
          color: "inherit",
          borderTopWidth: "1px",
        },
        ".scenarioEditor img": {
          borderStyle: "solid",
        },
        ".scenarioEditor textarea": {
          resize: "vertical",
        },
        ".scenarioEditor input::placeholder,.scenarioEditor textarea::placeholder":
          {
            opacity: "1",
            color: "#94a3b8",
          },
        '.scenarioEditor button,.scenarioEditor [role="button"]': {
          cursor: "pointer",
        },
        ".scenarioEditor table": {
          textIndent: "0",
          borderColor: "inherit",
          borderCollapse: "collapse",
        },
        ".scenarioEditor p,.scenarioEditor ol,.scenarioEditor ul,.scenarioEditor li,.scenarioEditor h1,.scenarioEditor h2,.scenarioEditor h3,.scenarioEditor h4,.scenarioEditor h5,.scenarioEditor h6":
          {
            fontSize: "inherit",
            fontWeight: "inherit",
          },
        ".scenarioEditor a": {
          color: "inherit",
          textDecoration: "inherit",
        },
        "button,input,optgroup,select,textarea": {},
        "button,select": {},
        ".scenarioEditor::-moz-focus-inner": {
          borderStyle: "none",
          padding: "0",
        },
        ".scenarioEditor:-moz-focusring": {
          outline: "1px dotted ButtonText",
        },
        ".scenarioEditor:-moz-ui-invalid": {
          boxShadow: "none",
        },
        ".scenarioEditor progress": {
          verticalAlign: "baseline",
        },
        ".scenarioEditor::-webkit-inner-spin-button,.scenarioEditor::-webkit-outer-spin-button":
          {
            height: "auto",
          },
        '.scenarioEditor [type="search"]': {
          WebkitAppearance: "textfield",
          outlineOffset: "-2px",
        },
        ".scenarioEditor::-webkit-search-decoration": {
          WebkitAppearance: "none",
        },
        ".scenarioEditor::-webkit-file-upload-button": {
          WebkitAppearance: "button",
          font: "inherit",
        },
        ".scenarioEditor summary": {
          display: "list-item",
        },
        ".scenarioEditor abbr[title]": {
          textDecoration: "underline dotted",
        },
        ".scenarioEditor b,.scenarioEditor strong": {
          fontWeight: "bolder",
        },
        ".scenarioEditor pre,.scenarioEditor code,.scenarioEditor kbd,.scenarioEditor samp":
          {
            fontFamily:
              'ui-monospace,SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace',
            fontSize: "1em",
          },
        ".scenarioEditor sub, .scenarioEditor sup": {
          fontSize: "75%",
          lineHeight: "0",
          position: "relative",
          verticalAlign: "baseline",
        },
        ".scenarioEditor sub": {
          bottom: "-0.25em",
        },
        ".scenarioEditor sup": {
          top: "-0.5em",
        },
        "img,svg,video,canvas,audio,iframe,embed,object": {},
        "img,video": {},
      };
    },
    theme: {
      extend: {
        colors: {
          primary: customTheme?.colors?.primary ?? {
            50: "#f0fdf4",
            100: "#dcfce7",
            200: "#bbf7d0",
            300: "#86efac",
            400: "#4ade80",
            500: "#22c55e",
            600: "#16a34a",
            700: "#15803d",
            800: "#166534",
            900: "#14532d",
          },
          gray: {
            50: "#f8fafc",
            100: "#f1f5f9",
            200: "#e2e8f0",
            300: "#cbd5e1",
            400: "#94a3b8",
            500: "#64748b",
            600: "#475569",
            700: "#334155",
            800: "#1e293b",
            900: "#0f172a",
          },
        },
      },
    },
  });

  return (
    <Box rootStyle={`bg-gray(100) p-4`} className="scenarioEditor">
      <Box rootStyle={`grid grid-cols-12 w-full gap-1`}>
        <Box rootStyle={`col-span-2`}>
          <Select placeholderText="keywords" options={["Given", "Then"]} />
        </Box>
        <Box rootStyle={`col-span-10`}>
          <Select
            placeholderText="select step"
            options={[
              "The value should be {string} in the {string}",
              "The text should be {string} in the {string}",
              "The popup text should be {string}",
            ]}
          />
        </Box>
      </Box>
    </Box>
  );
};

if (import.meta.env.MODE === "production")
  register(ScenarioEditor, "scenario-editor", ["theme"]);
