import { tw, apply } from 'https://cdn.skypack.dev/twind'
import { forwardRef } from 'preact/compat'

export const Box = forwardRef((props, ref) => {
  const { className = '', rootStyle = '', children, ...rest } = props

  const rootTw = apply`${rootStyle}`

  return (
    <div ref={ref} className={`${tw(rootTw)} ${className}`} {...rest}>
      {children}
    </div>
  )
})

Box.displayName = 'Box'
