import { tw, apply } from 'https://cdn.skypack.dev/twind'
import { forwardRef } from 'preact/compat'

const AtomName = forwardRef((props, ref) => {
  const { className, variant = 'sm', rootStyle, ...rest } = props

  const sizeMap = {
    xs: `text-xs py-1 px-2 rounded`,
    sm: apply`text-sm py-1.5 px-2 rounded-md`,
    md: apply`text-base py-2 px-4 rounded-lg`,
    lg: apply`text-lg py-2.5 px-5 rounded-lg`,
  }

  const rootTw = apply`${sizeMap[variant]}${rootStyle}`

  return (
    <div ref={ref} className={`${tw(rootTw)} ${className}`} {...rest}></div>
  )
})

AtomName.displayName = 'AtomName'

export { AtomName }
