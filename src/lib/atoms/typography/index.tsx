import { tw, apply } from 'https://cdn.skypack.dev/twind'

export const Text = (props) => {
  const { classNamed = '', rootStyle, children, ...rest } = props

  const rootTw = apply`text-gray-900 text-sm ${rootStyle}`

  return (
    <p className={`${tw(rootTw)} ${className}`} {...rest}>
      {children}
    </p>
  )
}

export const Heading = (props) => {
  const {
    as: Component = 'h1',
    rootStyle,
    size,
    children,
    className = '',
    ...rest
  } = props

  const sizeMap = {
    h1: apply`text-3xl`,
    h2: apply`text-2xl`,
    h3: apply`text-xl`,
    h4: apply`text-lg`,
    h5: apply`text-base`,
    h6: apply`text-sm`,
  }

  const rootTw = apply`
    text-gray-900 font-semibold text-xl
    ${sizeMap[size!]}
    ${rootStyle}
  `

  return (
    <Component className={tw(rootTw, className)} {...rest}>
      {children}
    </Component>
  )
}
