import { tw, apply } from 'https://cdn.skypack.dev/twind'
import { forwardRef } from 'preact/compat'
import { Box } from '../../layout/box'
import { Text } from '../../typography'

export const Input = forwardRef((props, ref) => {
  const {
    className = '',
    rootStyle = '',
    inputStyle = '',
    placeholderText = 'Placeholder Text..',
    type = 'text',
    size = 'md',
    errorElement,
    leftIcon,
    variant = 'outlined',
    error = false,
    required = false,
    disabled = false,
    ...rest
  } = props

  const variantMap = {
    outlined: apply`bg-white outline-none border([1.5px])`,
    filled: apply`bg(gray-100 white(focus:&)) border([1.5px] gray-100)`,
    flushed: apply`bg-white border(b-2 gray-300 primary-500(focus:&)) 
                     rounded-none focus:(outline-none)`,
  }

  const sizeMap = {
    sm: apply`text-xs py-1 px-2 rounded`,
    md: apply`text-sm py-1.5 px-2 rounded-md`,
    lg: apply`text-base py-2 px-4 rounded-lg`,
  }

  const paddingMap = {
    sm: apply`pl-8`,
    md: apply`pl-9`,
    lg: apply`pl-10`,
  }

  const inputTw = apply`
      block w-full text-gray-900
      placeholder::(text-gray-400 font-normal ${
        size === 'lg' ? 'text-base' : 'text-sm'
      })
      ${
        variant !== 'flushed'
          ? `focus:(outline-none border([1.5px] primary-500))`
          : ''
      }
      ${sizeMap[size]} 
      ${variantMap[variant]} 
      ${
        error
          ? `border([1.5px] red-500 red-500(focus:&)) 
            focus:(outline-none border([1.5px] red-400) ring([0.5px] red-400))`
          : ''
      }
      ${disabled ? 'bg-gray-200 border-none cursor-not-allowed' : ''}
      ${leftIcon && paddingMap[size]}
      ${inputStyle}
  `

  return (
    <Box rootStyle={`relative ${rootStyle}`}>
      {leftIcon && (
        <Box
          rootStyle={`absolute grid items-center pointer-events-none text-gray-400 h-full
            ${size === 'lg' ? ' pl-4' : ' pl-3'}`}
        >
          {leftIcon}
        </Box>
      )}
      <input
        type={type}
        className={`${tw(inputTw)} ${className}`}
        placeholder={placeholderText}
        disabled={disabled}
        required={required}
        ref={ref}
        {...rest}
      />
      {error && <Text>{error}</Text>}
      {errorElement && errorElement}
    </Box>
  )
})

Input.displayName = 'Input'
