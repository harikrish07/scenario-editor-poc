import { tw, apply } from "https://cdn.skypack.dev/twind";
import { useState, useRef, useEffect } from "preact/hooks";
import { Box } from "../../layout/box";
import { Input } from "../input";
import { useFocusWithin, useKeyPress } from "ahooks";

const Select = (props) => {
  const {
    className = "",
    rootStyle = "",
    inputStyle = "",
    listStyle = "",
    listItemStyle = "",
    placeholderText = "",
    options = [],
    ...rest
  } = props;

  const rootTw = apply`relative ${rootStyle}`;
  const listTw = apply`left-0 p-1 absolute top-10 bg-white shadow rounded w-full ${listStyle}`;
  const listItemTw = apply`outline-none list-none text-gray-900 rounded focus:(bg-primary-50)  px-1 py-0.5 
  hover:(bg-primary-50) ring(primary-200 focus-visible:2)) ${listItemStyle}`;

  const [showOptions, setShowOptions] = useState(false);
  const [selected, setSelected] = useState("");

  const inputRef = useRef(null);

  const listRef = useRef(null);

  const listItemRef = useRef([]);

  function handleSelect(option) {
    setSelected(option);
    setShowOptions(false);
  }

  useKeyPress(
    "enter",
    (e) => {
      setSelected(e.target.innerText);
      setShowOptions(false);
    },
    { target: listRef }
  );

  const isFocusWithin = useFocusWithin(inputRef, {
    onFocus: () => {
      setShowOptions(true);
    },
    onBlur: () => {
      setShowOptions(false);
    },
  });

  return (
    <Box ref={inputRef} rootStyle={rootTw} {...rest}>
      <Input
        inputStyle={inputStyle}
        placeholderText={placeholderText}
        type="text"
        value={selected}
      />
      <Box ref={listRef} rootStyle={"z-1000"}>
        {showOptions && (
          <>
            {options.length > 0 && (
              <ul className={`${tw(listTw)} ${className}`}>
                {options.map((option, index) => (
                  <li
                    ref={(el) => (listItemRef.current[option] = el)}
                    key={index}
                    className={`${tw(listItemTw)} ${className}`}
                    tabIndex={0}
                    onClick={() => handleSelect(option)}
                  >
                    {option}
                  </li>
                ))}
              </ul>
            )}
          </>
        )}
      </Box>
    </Box>
  );
};

Select.displayName = "Select";

export { Select };
