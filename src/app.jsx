import { useState } from "preact/hooks";
import { ScenarioEditor } from "./lib";
import "./app.css";

export function App() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <ScenarioEditor />
    </div>
  );
}
