# Website Styling Guide

We've used [twind](https://twind.dev/) to style the Website for the quicker development & for the better performance.

Twind is based on the tailwind css - https://v2.tailwindcss.com/, so we can use all the utilities classes available in the tailwindcss v2.

## Tailwind CSS Cheatsheet (v2.2) - https://nerdcave.com/tailwind-cheat-sheet

## Twind Quick Reference

```js
import { tw } from 'twind';
```

```jsx
<div className={tw`bg-white p-6 max-w-xs grid gap-2 justify-start`}>
  <button
    className={tw`text-primary-500 hover:(bg-gray-100 rounded sibling:(text-gray-400 bg-gray-100 place-items-center rounded)) sm:() md:(px-3) lg:() xl:() 2xl:() override:(text-gray-500)
    `}
  >
    A button
  </button>
  <p>Hello</p>
</div>
```

## The tw function

```js
tw`bg-gray-200 ${false && 'rounded'}`;
```

## Grouping

### Directive Grouping

```js
tw`border(2 black opacity-50 dashed)`;
```

### Variant Grouping - responsive and pseudo variants

```js
tw`
  bg-red-500 shadow-xs
  sm:(
    bg-red-600
    shadow-md
  )
  md:(bg-red-700 shadow)
  lg:(bg-red-800 shadow-xl)
`;
tw`w(1/2 sm:1/3 lg:1/6) p-2`;
```

### Mixed Groupings

```js
tw`sm:(border(2 black opacity-50 hover:dashed))`;
tw`border(md:(2 black opacity-50 hover:dashed))`;
tw`divide(y-2 blue-500 opacity(75 md:50))`;
tw`rotate(-3 hover:6 md:(3 hover:-6))`; // (Negated value)
```

### Self Reference

```js
tw`ring(& pink-700 offset(4 pink-200))`;
tw`bg-blue-500(hover:& focus:& active:&) rounded-full`;
```

## The apply function

The apply function is used to compose styles that can be later be overwritten in a tw call. Useful for component authors.

```js
import { apply, tw } from 'twind';

const btn = apply`bg-gray-200`;

<button class={tw`${btn}`}>bg-gray-200</button>
<button class={tw`${btn} bg-blue-200`}>bg-blue-500</button>
```

## CSS in Twind

### The css function (CSS-in-JS)

The css function allows us to write raw CSS in Twind, with support for pseudo-selectors, global styles, directives, animations, grouping syntax, and more.

```js
import { tw, css } from 'twind/css';

tw(
  css({
    '&::before': { content: '"🙁"' },
    '&::after': { content: '"😊"' },
  })
);

tw`
  sm:hover:${css({
    '&::before': { content: '"🙁"' },
    '&::after': { content: '"😊"' },
  })}
`;
```

## Configuration

```js
import { setup, strict, voidSheet } from 'twind';
import * as colors from 'twind/colors'; // Tailwind V2 colors

setup({
  theme: {
    extend: {
      gray: colors.trueGray,
      colors: { hotpink: '#FF00FF' },
      rotate: { 5: '5deg' },
    },
  },
});

// Advanced
setup({
  preflight: false, // do not include base style reset (default: use tailwind preflight)
  mode: strict, // throw errors for invalid rules: "strict", "warn" or "silent"- default is warn
  hash: true, // hash all generated class names (default: false)
  theme: {}, // define custom theme values (default: tailwind theme)
  darkMode: 'class', // use a different dark mode strategy (default: 'media')
  sheet: voidSheet, // use custom sheet (default: cssomSheet in a browser or no-op)
});
```

## Beyond Tailwind

Twind includes several directives, variants, and utilities beyond Tailwind:

### SYNTAX

- Custom syntax for grouping directives and variants (see grouping above)
- Important!

```js
<p className="text-red-500!">Hi</p>
```

### VARIANTS

- Every variant can be applied to every directive
- Dark mode is always available
- Most pseudo-classes can be used as variant or group-\* variant
- siblings: - General sibling combinator (& ~ \*)
- sibling: - Adjacent sibling combinator (& + \*)
- children:- Child combinator (& > \*)
- Inherited CSS Properties


## All variants & directives

```jsx

import { tw, apply } from 'twind';

//typescript

type InputProps = {
  className?: string;
  size: 'xs' | 'sm' | 'md' | 'lg';
};

export const ComponentName = (props: InputProps): JSX.Element => {
  const { className, size } = props;

  const sizeMap = {
    xs: apply`text-xs py-1 px-2 rounded`,
    sm: apply`text-sm py-1.5 px-2 rounded-md`,
    md: apply`text-base py-2 px-4 rounded-lg`,
    lg: apply`text-lg py-2.5 px-5 rounded-lg`,
  };

  const instanceStyles = apply`
  w(full sm:auto) text(lg uppercase gray-100)
  bg-purple(800 700(hover:& focus:&)) ring(purple-400 focus-visible:4))
  px(6 sm:8) py(2 md:6) rounded-full transition-colors duration-300 flex
  hover:()
  focus:()
  group-hover:()
  active:()
  before::()
  after::()
  checked:()
  first:()
  last:()
  odd:()
  even:()
  sibling:()
  siblings:()
  children:()
  sm:(grid) md:() lg:() xl:() 2xl:()
  override:()
  dark:()
  ${true && 'override:(bg-red)'}
  ${sizeMap[size]}
  `;
  return <div className={tw(instanceStyles, className)}></div>;
};

```